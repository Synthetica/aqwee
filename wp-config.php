<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'synthetica_aqwee');

/** Имя пользователя MySQL */
define('DB_USER', 'Synthetica');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'V@ld1s81');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3RG9P^Eaqu,iQCER@/oMt`H.4|}p2z+jT%oBZ?fo{-Uc_jd4FBd;ab^&<(m,O|&2');
define('SECURE_AUTH_KEY',  'v^_t|!U]s+-cqKA4zFX&w~||f G-EBL>&||~2b3j:x(;@44Umj: 2O@~J#$N@*n`');
define('LOGGED_IN_KEY',    'Bg3h[:LLP>-K-j-,:~hv,/Ve!&-GC1 +i--`dh4UGwsot4pSmK)tc&X;Li*CjU$4');
define('NONCE_KEY',        '2-_NCjv2U_::Fb9Oy1+=QXfA.y|6%IG=irM)]x<J?;^Oke_fWC>b-#e|UUh?(:|`');
define('AUTH_SALT',        '+WSwPs$YQcWNT(- >{~|+uj.Ina90AvH1tChj19y5_qIRRD+X+^KMg|#F/Oh8_2?');
define('SECURE_AUTH_SALT', 'Eb80u}6^5Ib/nxuo,~P%nB*f,L`sk</%L!F<pP:7:8c~&Q5}/K.a0@dab?.`88oa');
define('LOGGED_IN_SALT',   'F$>[.$[dL+}:=yo>JlwceG25g^^ Owd{.[Qbre~@8b|uLy5>ndow!0f}V+^$[5ph');
define('NONCE_SALT',       'BBw!k?ZuseOcdG&=doMlUMOQ>-++,D]5,YSEKR-.be9xAmx?,q)0qZ;!<Myp|;pk');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
