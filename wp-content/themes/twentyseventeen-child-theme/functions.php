<?php

require_once 'includes/snth-lib.php';
require_once 'includes/class-snth-child.php';

if ( class_exists( 'WooCommerce' ) ) {
    require_once 'includes/class-snth-woo.php';
}
