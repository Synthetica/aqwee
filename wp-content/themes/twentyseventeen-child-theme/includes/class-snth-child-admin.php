<?php
if (! defined( 'ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (! class_exists('SNTH_Child_Admin')) {
    class SNTH_Child_Admin
    {
        /**
         * The single instance of the class.
         *
         * @var SNTH_Child_Admin
         */
        protected static $_instance = null;

        /**
         * Main SNTH_Child_Admin Instance.
         *
         * @return SNTH_Child_Admin - Main instance.
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * SNTH_Child_Admin Constructor.
         * @since 0.0.1
         */
        private function __construct()
        {
            add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'), 1);
            add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts') ,1);

            $this->actions();
        }

        /**
         * Enqueueing admin styles
         */
        public function enqueue_admin_styles()
        {

        }

        /**
         * Enqueueing admin scripts
         */
        public function enqueue_admin_scripts()
        {
            wp_register_script(SNTH_Child::$prefix . '-admin-main', SNTH_Child::$admin_main_js, array('jquery'), SNTH_Child::$version, true);
            wp_enqueue_script(SNTH_Child::$prefix . '-admin-main');
        }

        /**
         * Add or remove actions
         */
        public function actions()
        {
            add_action('wp_dashboard_setup', array($this, 'remove_dashboard_widgets' ));
            remove_action('welcome_panel', 'wp_welcome_panel');
        }

        /**
         * Remove dashboard widgets
         */
        public function remove_dashboard_widgets()
        {
            // Globalize the metaboxes array, this holds all the widgets for wp-admin
            global $wp_meta_boxes;

            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
            unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
            unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
        }
    }

    /**
     * Main instance of SNTH_Child_Admin.
     * @return SNTH_Child_Admin
     */
    function SNTH_Child_Admin() {
        return SNTH_Child_Admin::instance();
    }

    // Global for backwards compatibility.
    $GLOBALS['snth_child_admin'] = SNTH_Child_Admin();
}
