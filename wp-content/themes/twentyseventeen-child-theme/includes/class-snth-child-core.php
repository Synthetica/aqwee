<?php
if (! defined( 'ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (! class_exists('SNTH_Child_Core')) {
    class SNTH_Child_Core
    {
        /**
         * The single instance of the class.
         *
         * @var SNTH_Child_Core
         */
        protected static $_instance = null;

        /**
         * Main SNTH_Child_Core Instance.
         *
         * @return SNTH_Child_Core - Main instance.
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * SNTH_Child_Core Constructor.
         * @since 0.0.1
         */
        private function __construct()
        {
            add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'));
            add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

            $this->actions();
        }

        /**
         * Enqueueing styles
         */
        public function enqueue_styles()
        {
            if (SNTH_Child::$is_child) {
                wp_enqueue_style(SNTH_Child::$prefix . '-parent', get_template_directory_uri() . '/style.css');
            }

            wp_register_style(SNTH_Child::$prefix . '-fontawesome', SNTH_Child::$vendors . '/fontawesome/css/font-awesome.min.css', array(), '4.7.0');
            wp_enqueue_style(SNTH_Child::$prefix . '-fontawesome');

            wp_register_style(SNTH_Child::$prefix . '-main', SNTH_Child::$main_css, false, SNTH_Child::$version);
            wp_enqueue_style(SNTH_Child::$prefix . '-main');
        }

        /**
         * Enqueueing scripts
         */
        public function enqueue_scripts()
        {
            wp_register_script(SNTH_Child::$prefix . '-main-js', SNTH_Child::$main_js, array('jquery', 'divi-custom-script'), SNTH_Child::$version, true);
            wp_enqueue_script(SNTH_Child::$prefix . '-main-js');

//            wp_localize_script(SNTH_Child::$prefix . '-main-js', SNTH_Child::$prefix . 'SnthDivi', array(
//                'ajaxurl' => admin_url('admin-ajax.php')
//            ));
        }

        /**
         * Add or remove actions
         */
        public function actions()
        {
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('wp_print_styles', 'print_emoji_styles');
        }
    }

    /**
     * Main instance of SNTH_Child_Core.
     * @return SNTH_Child_Core
     */
    function SNTH_Child_Core() {
        return SNTH_Child_Core::instance();
    }

    // Global for backwards compatibility.
    $GLOBALS['snth_child_core'] = SNTH_Child_Core();
}
