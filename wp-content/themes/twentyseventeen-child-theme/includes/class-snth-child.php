<?php
if (! defined( 'ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (! class_exists('SNTH_Child')) {
    class SNTH_Child
    {
        /**
         * The single instance of the class.
         *
         * @var SNTH_Child
         */
        protected static $_instance = null;
        protected static $_fw_version = '1.1.0';

        public static $config = array();
        public static $is_child = true;

        public static $prefix = 'snth';
        public static $version;
        public static $path;
        public static $parent_path = null;
        public static $inc;

        public static $uri;
        public static $parent_uri = null;

        public static $assets;
        public static $css;
        public static $js;
        public static $img;
        public static $vendors;

        public static $main_css;
        public static $main_js;
        public static $admin_main_css;
        public static $admin_main_js;

        /**
         * Main SNTH_Child Instance.
         *
         * @return SNTH_Child - Main instance.
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * SNTH_LPTN Constructor.
         * @since 0.0.1
         */
        private function __construct()
        {
            $this->config();
            $this->define_constants();

            spl_autoload_register(array($this, 'autoload'));

            $this->includes();

            if ( is_admin() ) {
                $this->admin_includes();
            }
        }

        private function config()
        {
            $config_file = get_stylesheet_directory() . '/config.json';

            if (file_exists($config_file)) {
                $config = json_decode(file_get_contents($config_file), true);
                self::$config = array_merge(self::$config, $config);
            }

            if (isset(self::$config['prefix']) && self::$config['prefix']) {
                self::$prefix = self::$config['prefix'];
            }

//            var_dump(self::$config);
//            die();
        }

        public function includes()
        {
            require_once self::$inc . '/class-snth-child-core.php';
        }

        public function admin_includes()
        {
            require_once self::$inc . '/class-snth-child-admin.php';
        }

        public function define_constants()
        {
            self::$path = get_stylesheet_directory();
            self::$uri = get_stylesheet_directory_uri();
            self::$inc = self::$path . '/includes';

            if(isset (self::$config['prefix']) && self::$config['prefix']) {
                self::$prefix = self::$config['prefix'];
            }

            $theme = wp_get_theme();
            self::$version = $theme->get('Version');

            if (self::$is_child) {
                self::$parent_path = get_template_directory();
                self::$parent_uri = get_template_directory_uri();
            }

            self::$assets = self::$uri . '/assets';
            self::$css = self::$assets . '/css';
            self::$js = self::$assets . '/js';
            self::$img = self::$assets . '/img';
            self::$vendors = self::$assets . '/vendors';

            self::$main_css = self::$css . '/main.css';
            self::$main_js = self::$js . '/main.js';

            self::$admin_main_css = self::$css . '/admin-main.css';
            self::$admin_main_js = self::$js . '/admin-main.js';
        }

        /**
         * Framework class autoloader
         *
         * @param $class
         */
        public function autoload($class_name)
        {
            if (class_exists($class_name)) {
                return;
            }

            $class_path = self::$inc . DIRECTORY_SEPARATOR . 'includes'
                . DIRECTORY_SEPARATOR . 'class-' . strtolower(str_replace( '_', '-', $class_name )) . '.php';

            if ( file_exists( $class_path ) ) {
                include $class_path;
            }
        }
    }

    /**
     * Main instance of SNTH_Child.
     * @return SNTH_Child
     */
    function SNTH_Child() {
        return SNTH_Child::instance();
    }

    // Global for backwards compatibility.
    $GLOBALS['snth_child'] = SNTH_Child();
}
