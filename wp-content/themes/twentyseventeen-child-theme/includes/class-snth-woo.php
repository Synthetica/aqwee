<?php
if (! defined( 'ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (! class_exists('SNTH_Woo')) {
    class SNTH_Woo
    {
        /**
         * The single instance of the class.
         *
         * @var SNTH_Child
         */
        protected static $_instance = null;

        /**
         * Main SNTH_Woo Instance.
         *
         * @return SNTH_Woo - Main instance.
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * SNTH_Woo Constructor.
         * @since 0.0.1
         */
        private function __construct()
        {
            $this->define_constants();

            $this->includes();

            $this->template_loader();

            if ( is_admin() ) {
                $this->admin_includes();
            }

            //add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'));
            //add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        }

        public function includes(){}

        public function admin_includes() {}

        public function define_constants() {}

        public function template_loader() {}

        /**
         * Enqueueing styles
         */
        public function enqueue_styles() {
            wp_register_style(SNTH_Child::$prefix . '-slick', SNTH_Child::$vendors . '/slick/slick.css', false, '1.6.0');
            wp_enqueue_style(SNTH_Child::$prefix . '-slick');
        }

        /**
         * Enqueueing scripts
         */
        public function enqueue_scripts()
        {
            wp_register_script(SNTH_Child::$prefix . '-slick', SNTH_Child::$vendors . '/slick/slick.min.js', array('jquery'), '1.6.0', true);
            wp_enqueue_script(SNTH_Child::$prefix . '-slick');

            $scriptSlick = 'jQuery("#snth-product-images-slider").slick({
                                adaptiveHeight: !0,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                prevArrow: \'<a class="slick-prev"><i class="fa fa-angle-left"></i></a>\',
                                nextArrow: \'<a class="slick-next"><i class="fa fa-angle-right"></i></a>\',
                                dots: 0,
                                fade: !0
                            });';
            wp_add_inline_script(SNTH_Child::$prefix . '-slick', $scriptSlick);
        }

        /**
         * Add or remove actions
         */
        public function actions()
        {

        }
        
        /**
         * Add zoom 
         */
    }

    /**
     * Main instance of SNTH_Woo.
     * @return SNTH_Woo
     */
    function SNTH_Woo() {
        return SNTH_Woo::instance();
    }

    // Global for backwards compatibility.
    $GLOBALS['snth_woo'] = SNTH_Woo();
}
