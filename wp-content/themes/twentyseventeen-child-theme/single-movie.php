<?php
/**
 * The template for displaying all movies
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<?php get_header('movie'); ?>
    <main id="main" class="site-main" role="main">

        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/movie/content');

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->
<?php get_footer('movie');
