<?php
/**
 * Template part for displaying movies
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php
$sourceMp4 = '';
$sourceOgg = '';
$sourceWebm = '';

if (get_post_meta( get_the_id(), '_url_mp4', true )) {
    $sourceMp4 = '<source src="' . get_post_meta( get_the_id(), '_url_mp4', true ) . '" type="video/mp4">';
}

if (get_post_meta( get_the_id(), '_url_mp4', true )) {
    $sourceOgg = '<source src="' . get_post_meta( get_the_id(), '_url_ogg', true ) . '" type="video/ogg">';
}

if (get_post_meta( get_the_id(), '_url_mp4', true )) {
    $sourceWebm = '<source src="' . get_post_meta( get_the_id(), '_url_webm', true ) . '" type="video/webm">';
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <video width="100%" height="100%" controls autoplay>
        <?= $sourceMp4 ?>
        <?= $sourceOgg ?>
        <?= $sourceWebm ?>
        Your browser does not support the video tag.
    </video>
</article><!-- #post-## -->